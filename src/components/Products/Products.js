import React, { Component } from 'react'
import Loader from 'react-loader-spinner'
import ProductList from '../ProductList/ProductList'
import Nodata from '../NoData/Nodata'
import Errpage from '../Errpage/Errpage'
import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css'

import '../../App.css'

class Products extends Component {

    addProduct = () => {
        this.props.addProductFn()
    }

    changingInput = (event) => {

        const name = event.target.name
        const value = event.target.value

        this.props.addProductDetailsInput(name, value)
    }

    saveFn = () => {
        this.props.onSaveFn()
    }

    deleteProduct = (id) => {
        this.props.productSelected(id)
    }

    render() {


        const [productsData, fetchSuccessful, isLoading, errMessage, addProduct] = this.props.products

        const addItem = (
            <div className='col-12 col-sm-6 col-md-3 d-flex flex-column justify-content-center align-items-center decCard m-1 text-center' >

                <span className='text-white fw-bold bg-primary rounded mt-2' onClick={this.addProduct}> <i className="fa-solid fa-plus size"></i> </span> <br />
                <span>Add Product</span>

            </div>
        )

        const addItemForm = (

            <div className='col-12 col-sm-6 col-md-3 d-flex flex-column decCard m-1' >
                <label className='fw-bold'>Title</label>
                <input type='text' name='title' className='rounded' onChange={this.changingInput} />
                <label className='fw-bold'>Description</label>
                <textarea rows='3' name='description' className='rounded' onChange={this.changingInput}></textarea>
                <label className='fw-bold'>Price</label>
                <input type='number' name='price' className='rounded' onChange={this.changingInput} />
                <label className='fw-bold'>Image URL</label>
                <input type='url' name='image' className='rounded' onChange={this.changingInput} />
                <label className='fw-bold'>Category</label>
                <input type='text' name='category' className='rounded' onChange={this.changingInput} />
                <div className='mt-1'>
                    <button className='btn-primary' onClick={this.saveFn}>Save</button>
                </div>
            </div>
        )

        const render = productsData.length > 0 ?
            (<div className='container p-0'>
                <div className='row d-flex justify-content-center'>
                    {productsData.map((eachProduct) => { return <ProductList key={eachProduct.id} product={eachProduct} changeDeleteProducts={this.deleteProduct} /> })}

                    {addProduct ? addItemForm : addItem}

                </div>

            </div>) :
            <Nodata message={errMessage} />


        const finalRender =  isLoading ? <div className='d-flex justify-content-center align-items-center vh-100'> <Loader /> </div> : fetchSuccessful ? render : <Errpage message={errMessage} />
        
        return this.props.processing ?<div className='d-flex justify-content-center align-items-center vh-100'> <Loader /> </div>  : finalRender
    }

}

export default Products