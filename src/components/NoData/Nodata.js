import React from 'react'
import { Link } from 'react-router-dom'
import '../../App.css'

function Nodata() {
   
  return (
    <div className='d-flex flex-column justify-content-center aligen-items-center nodata text-center'>
       <h1 className='text-danger fw-bold decCard'>NO DATA FOUND</h1>
       <Link to='/' >
           <button className='btn-warning backBtn'>Back</button>
       </Link>
    </div>
  )
}

export default Nodata