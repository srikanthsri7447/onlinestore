import React from 'react'
import { Link } from 'react-router-dom'

import '../../App.css'

function Home() {
  
  return (
    <div className='d-flex flex-column justify-content-center align-items-center homeContainer'>
        <h2 className='bg-light'>Latest Trending Fashions, Electronics & Much more.. </h2>
        <Link to='/products'>
        <button className='btn-primary'>Explore</button>
        </Link>
    </div>
  )
}

export default Home