import React from 'react'
import { Link } from 'react-router-dom'
import '../../App.css'

function ProductList(props) {

  const { id, title, description, price, image, category } = props.product

  function changeDeleteFn() {
    props.changeDeleteProducts(props.product.id)
  }

  return (
    <Link to={`/product/${id}`} onClick={changeDeleteFn} className='col-12 col-sm-6 col-md-3 d-flex flex-column justify-content-between align-items-center decCard m-1 text-center' >

      <span className='text-danger'> Trending {category}</span> <br />
      <div className='imgSize'>
        <img src={image} alt='product' className='w-100' /> <br />
      </div>
      <span className='fw-bold'>{title}</span> <br />
      <span className='desc'>{description}</span> <br />
      <span className='text-danger'>Price: ${price}</span>

    </Link>
  )
}

export default ProductList