import React from 'react'
import { Link } from 'react-router-dom'

function Header(props) {
    const isLogged = props.isLogged
    const updateToggleLogger = props.updateToggleLogger
    const toggleSign = isLogged ? 'Logout' : 'Signup'

    const logout = () => {
        updateToggleLogger()
    }


    return (
        <div>
            <nav className="navbar navbar-expand-md navbar-light bg-warning ">
                <div className="container-fluid">
                    <Link to='/' className='fw-bold'>SHOP ON</Link>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>

                    <div className="collapse navbar-collapse justify-content-end" id="navbarNav">
                        <ul className="navbar-nav flex-row justify-content-center mt-1">
                            <li className="nav-item me-3">
                                <Link to='/'>Home</Link>
                            </li>
                            <li className="nav-item me-3">
                                <Link to='/products'>Products</Link>
                            </li>
                            <li className="nav-item me-3">
                                <Link to='/signup' onClick={logout}>{toggleSign}</Link>
                            </li>
                            <li>
                                <i className="fa-solid fa-cart-plus"></i>

                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    )
}

export default Header