import React from 'react'
import { Link } from 'react-router-dom'
import '../../App.css'

function Errpage(props) {
    const errMsg = props.message
  return (
    <div className='d-flex flex-column justify-content-center aligen-items-center nodata text-center errPage'>
       <h1 className='text-danger fw-bold decCard'>{errMsg}</h1>
       <Link to='/' >
           <button className='btn-warning backBtn'>Back</button>
       </Link>
    </div>
  )
}

export default Errpage