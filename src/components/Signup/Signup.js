import React, { Component } from 'react'
import { Redirect } from 'react-router-dom';
import validator from 'validator';
import './Signup.css'

class Signup extends Component {

    constructor(props) {
        super(props)

        this.state = {
            firstName: '',
            userIcon: <i className="fa-solid fa-venus-mars"></i>,
            lastName: '',
            email: '',
            gender: '',
            role: '',
            password: '',
            passwordType: 'password',
            cnfrmPasswordType: 'password',
            reEnterPassword: '',
            age: 0,
            error: {},
            firstNameValid: false,
            lastNameValid: false,
            emailValid: false,
            dateValid: false,
            passwordValid: false,
            showcnfrmPassword: false,
            isPasswordMatched: false,
            isChecked: false,
            showPassword: false,
            isSubmited: false

        }
    }

    errorMessages = (name, err) => {

        this.setState((prevState) => prevState.error[name] = err)
    }

    validateElement = (name, value) => {

        let firstNameValid = this.state.firstNameValid
        let lastNameValid = this.state.lastNameValid
        let emailValid = this.state.emailValid
        let passwordValid = this.state.passwordValid

        switch (name) {
            case 'firstName':
                firstNameValid = (validator.isAlpha(value) && value.length >= 2)
                if (!firstNameValid) {
                    const msg = '*Enter a valid name'
                    this.errorMessages(name, msg)
                    break;
                }
                else {
                    this.errorMessages(name, '')
                    this.setState({
                        firstNameValid: firstNameValid
                    })
                }
                break;
            case 'lastName':
                lastNameValid = (validator.isAlpha(value) && value.length >= 2)
                if (!lastNameValid) {
                    const msg = '*Enter a valid name'
                    this.errorMessages(name, msg)
                    break;
                }
                else {
                    this.errorMessages(name, '')
                    this.setState({
                        lastNameValid: lastNameValid
                    })
                }
                break;
            case 'email':
                emailValid = (validator.isEmail(value))
                if (!emailValid) {
                    const msg = '*Enter a valid Email'
                    this.errorMessages(name, msg)
                    break;
                }
                else {
                    this.errorMessages(name, '')
                    this.setState({
                        emailValid: emailValid
                    })
                }
                break;
            case 'password':
                passwordValid = (validator.isStrongPassword(value))
                if (!passwordValid) {
                    const msg = '*Password must contains 1uppercase character, 1 lowercase character, 1digit/number, 1special character, minimum 8 characters'
                    this.errorMessages(name, msg)
                    break;
                }
                else {
                    this.errorMessages(name, '')
                    this.setState({
                        passwordValid: passwordValid
                    })
                }
                break;
            default:
                break;
        }
    }

    validateInput = (event) => {

        let name = event.target.name
        let value = event.target.value.trim()

        this.setState(
            { [name]: value },
            () => { this.validateElement(name, value) }
        )

    }

    calculateAge = (event) => {

        if (validator.isDate(event.target.value)) {

            const date = new Date();
            const year = date.getFullYear();

            const dob = event.target.value;
            const dobDate = new Date(dob);
            const dobYear = dobDate.getFullYear();

            const age = year - dobYear;

            this.setState({
                age: age,
                dateValid: true
            })
            this.errorMessages(event.target.name, '')

        }
        else {
            this.setState({
                age: 0
            })
            this.errorMessages(event.target.name, '*Enter Valid Date!')
        }


    }

    checkPassword = (event) => {
        let name = event.target.name
        let cnfrmPassword = event.target.value
        let passwordMatch = (cnfrmPassword === this.state.password)

        if (passwordMatch) {
            this.errorMessages(name, '')
            this.setState({
                isPasswordMatched: passwordMatch,
                reEnterPassword: cnfrmPassword
            })
        }
        else {
            this.errorMessages(name, '*Password not Matched')
        }
    }

    updateSelector = (event) => {
        let name = event.target.name
        let value = event.target.value
        
        if(name==='gender'){
            if(!value){
                this.setState({
                    userIcon : <i className="fa-solid fa-venus-mars"></i>
                })
            }
            if(value === 'Male'){
                this.setState({
                    userIcon : <i className="fa-solid fa-mars"></i>
                })
            }
            if(value === 'Female'){
                this.setState({
                    userIcon : <i className="fa-solid fa-venus"></i>
                })
            }
            if(value === 'other'){
                this.setState({
                    userIcon : <i className="fa-solid fa-genderless"></i>
                })
            }
        }

        if (value) {
            this.setState({
                [name]: value
            })
            this.errorMessages(name, '')
        }
        else {
            this.errorMessages(name, `*Please update ${name}`)
        }


    }

    togglePassword = () => {
        let passwordType = this.state.passwordType
        let showPassword = this.state.showPassword

        if (passwordType === 'password') {
            this.setState({
                showPassword: !showPassword,
                passwordType: 'text'
            })
        }
        else {
            this.setState({
                showPassword: !showPassword,
                passwordType: 'password'
            })
        }

    }
    togglecnfrmPassword = () => {
        let cnfrmPasswordType = this.state.cnfrmPasswordType
        let showcnfrmPassword = this.state.showcnfrmPassword

        if (cnfrmPasswordType === 'password') {
            this.setState({
                showcnfrmPassword: !showcnfrmPassword,
                cnfrmPasswordType: 'text'
            })
        }
        else {
            this.setState({
                showcnfrmPassword: !showcnfrmPassword,
                cnfrmPasswordType: 'password'
            })
        }

    }
    validCheckbox = (event) => {

        let name = event.target.name
        let check = event.target.checked
        this.setState({
            isChecked: check
        })

        if (check) {
            this.errorMessages(name, '')
        }
        else {
            this.errorMessages(name, '*Please agree Terms and Conditions before Submit')
        }
    }

    submitForm = (event) => {
        event.preventDefault();
        let firstNameValid = this.state.firstNameValid
        let lastNameValid = this.state.lastNameValid
        let gender = this.state.gender
        let role = this.state.role
        let age = this.state.age
        let dateValid = this.state.dateValid
        let emailValid = this.state.emailValid
        let passwordValid = this.state.passwordValid
        let isPasswordMatched = this.state.isPasswordMatched
        let isChecked = this.state.isChecked
        let reEnterPassword = this.state.reEnterPassword


        if (firstNameValid &&
            lastNameValid &&
            dateValid &&
            emailValid &&
            passwordValid &&
            isPasswordMatched &&
            isChecked
        ) {
            this.setState({
                isSubmited: true,
            })
            this.props.changeLogger();
        }
        if (!firstNameValid && this.state.firstName.length === 0) {
            this.errorMessages('firstName', '*First Name Require')
        }
        if (!lastNameValid && this.state.lastName.length === 0) {
            this.errorMessages('lastName', '*Last Name Require')
        }
        if (!emailValid && this.state.email.length === 0) {
            this.errorMessages('email', '*Email Require')
        }
        if (gender.length === 0) {
            this.errorMessages('gender', '*Gender Require')
        }
        if (role.length === 0) {
            this.errorMessages('role', '*Role Require')
        }
        if (!dateValid) {
            this.errorMessages('age', '*Age Require')
        }
        if (age === 0) {
            this.errorMessages('age', '*Age Require')
        }
        if (!passwordValid && this.state.password.length === 0) {
            this.errorMessages('password', '*Password Require')
        }
        if (reEnterPassword.length === 0) {
            this.errorMessages('reEnter', '*Confirm Password Require')
        }
        if (!isChecked) {
            this.errorMessages('checkbox', '*Accept Terms and Conditions Require')
        }

    }

    render() {

        const { age, error, isSubmited, showPassword, passwordType, cnfrmPasswordType, showcnfrmPassword, userIcon } = this.state

        if (isSubmited) {
            return  <Redirect to='/products'/>
        }
        else {
            return (
                <div className='container appContainer'>
                    <div className='row subAppContainer'>

                        <div className='col-10 formContainer pr-md-0'>

                            <form onSubmit={this.submitForm} noValidate="noValidate">

                                <div className='inputContainer mt-3'>
                                    <div className='inputSubContainer'>
                                        {userIcon}
                                        <input id='firstName' name='firstName' type='text' className='input' placeholder='Enter First Name' onChange={this.validateInput} />
                                    </div>
                                    <span className='err'>{error.firstName}</span>
                                </div>

                                <div className='inputContainer'>
                                    <div className='inputSubContainer'>
                                        {userIcon}
                                        <input id='lastName' type='text' name='lastName' className='input' placeholder='Enter Last Name' onChange={this.validateInput} />
                                    </div>
                                    <span className='err'>{error.lastName}</span>
                                </div>

                                <div className='inputContainer alineColumn'>
                                    <select className='gender' name='gender' onChange={this.updateSelector}>
                                        <option value=''>Gender</option>
                                        <option value='Female'>Female</option>
                                        <option value='Male'>Male</option>
                                        <option value='other'>I prefer not to say</option>
                                    </select>
                                    <span className='err'>{error.gender}</span>
                                </div>

                                <div className='inputContainer ageCnt'>
                                    <div className='inputSubContainer'>
                                        <input id='dob' type='date' name='age' className='dobInput' onChange={this.calculateAge} />
                                        <span className='age'>Age {age}</span>
                                    </div>
                                    <span className='err'>{error.age}</span>
                                </div>

                                <div className='inputContainer'>
                                    <div className='inputSubContainer'>
                                        <i className="fa-solid fa-at"></i>
                                        <input id='email' type='text' name='email' className='input' placeholder='Enter Email' onChange={this.validateInput} />
                                    </div>
                                    <span className='err'>{error.email}</span>
                                </div>

                                <div className='inputContainer'>
                                    <div className='inputSubContainer'>
                                    <i className="fa-solid fa-user-tie"></i>
                                        <select id='role' name='role' className='input' onChange={this.updateSelector}>
                                            <option value=''>Role</option>
                                            <option value='developer'>Developer</option>
                                            <option value='senior developer'>Senior Developer</option>
                                            <option value='lead engineer'>Lead Engineer</option>
                                            <option value='CTO'>CTO</option>
                                        </select>
                                    </div>
                                    <span className='err'>{error.role}</span>
                                </div>

                                <div className='inputContainer'>
                                    <div className='inputSubContainer'>
                                        <i className="fa-solid fa-key"></i>
                                        <input id='password' type={passwordType} name='password' className='input' placeholder='Password' onChange={this.validateInput} />
                                        {showPassword ? <i className="fa-solid fa-eye-slash" onClick={this.togglePassword}></i> : <i className="fa-solid fa-eye" onClick={this.togglePassword}></i>}

                                    </div>
                                    <span className='err'>{error.password}</span>
                                </div>

                                <div className='inputContainer'>
                                    <div className='inputSubContainer'>
                                        <i className="fa-solid fa-key"></i>
                                        <input id='reEnterPassword' name='reEnter' type={cnfrmPasswordType} className='input' placeholder='Re-enter Password' onChange={this.checkPassword} />
                                        {showcnfrmPassword ? <i className="fa-solid fa-eye-slash" onClick={this.togglecnfrmPassword}></i> : <i className="fa-solid fa-eye" onClick={this.togglecnfrmPassword}></i>}
                                    </div>
                                    <span className='err'>{error.reEnter}</span>
                                </div>

                                <div className='inputContainer'>
                                    <div className='checkboxSubContainer'>
                                        <input type='checkbox' id='checkBox' name='checkbox' className='checkBox' onChange={this.validCheckbox} />
                                        <label htmlFor='checkBox' className='checkboxLabel'> Agree Terms and Conditions </label>
                                    </div>
                                    <span className='err'>{error.checkbox}</span>
                                </div>

                                <button id='submitBtn' type='submit' className='submitBtn mb-3'>Submit</button>

                            </form>

                            <img
                                src='https://media.istockphoto.com/photos/businessman-filling-online-registration-form-picture-id1013435204?b=1&k=20&m=1013435204&s=170667a&w=0&h=AyuR-G-C9lPYAIf6noX4xVzbwwuH83jxIeAUz_w3gIs='
                                alt='signUpImage'
                                className='signUpImage'
                            />

                        </div>

                    </div>
                    
                </div>
            )
        }

    }
}
export default Signup