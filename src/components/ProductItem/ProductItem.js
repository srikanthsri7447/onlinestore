import React, { Component } from 'react'
import Loader from 'react-loader-spinner'
import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css'
import Nodata from '../NoData/Nodata'
import Errpage from '../Errpage/Errpage'
import { Redirect } from 'react-router-dom'


import '../../App.css'

class ProductItem extends Component {
  constructor(props) {
    super(props)
    this.state = {
      productDetails: [],
      fetchSuccesfull: false,
      isLoading: true,
      errMessage: ''
    }
  }

  componentDidMount() {
    this.getDetails()
  }


  getDetails = () => {
    const { match } = this.props
    const { id } = match.params

    const details = this.props.productsData.filter((eachProduct) => eachProduct.id === Number(id))

    this.setState({
      productDetails: details[0],
      isLoading: false,
      fetchSuccesfull: true
    })
  }

  increaseQuantity = () => {

    const id = this.state.productDetails.id

    const orderQnt = (!this.props.orderQuantity) ? 0 : this.props.orderQuantity

    orderQnt === 0 ? this.props.changeQuantity(id, 1) : this.props.changeQuantity(id, (orderQnt + 1))


  }

  decreaseQuantity = () => {

    const id = this.state.productDetails.id

    const orderQnt = isNaN(this.props.orderQuantity[id]) ? 0 : this.props.orderQuantity[id]

    if (orderQnt > 0) {

      this.props.changeQuantity(id, (orderQnt - 1))

    }

  }

  deleteProduct = () => {
    const id = this.state.productDetails.id
    this.props.deleteProductFn(id)
  }

  updateSelector = () => {
    const id = this.state.productDetails.id
    this.props.updateSelector(id)
  }

  changeTitle = (event) => {
    const id = this.state.productDetails.id
    const value = event.target.value
    this.props.updateTitle(id,value)
  }

  changeDescription = (event) => {
    const id = this.state.productDetails.id
    const value = event.target.value
    this.props.updateDescription(id,value)
  }

  changePrice = (event) => {
    const id = this.state.productDetails.id
    const value = event.target.value
    this.props.updatePrice(id,value)
  }

  changeImage = (event) => {
    const id = this.state.productDetails.id
    const value = event.target.value
    this.props.updateImage(id,value)
  }

  changeCategory = (event) => {
    const id = this.state.productDetails.id
    const value = event.target.value
    this.props.updateCategory(id,value)
  }

  SaveUpdate = () => {
    const id = this.state.productDetails.id

    this.props.saveUpdate(id)
  }


  render() {

    const { productDetails, isLoading, errMessage, fetchSuccesfull } = this.state

    const { id, image, title, price, description, category } = productDetails

    const selector = this.props.selector

    const orderQnt = isNaN(this.props.orderQuantity[id]) ? 0 : this.props.orderQuantity[id]

    const subEditor = !selector ?

      (<div className='d-flex flex-column flex-lg-row justify-content-center align-items-center mt-3 decCard'>

        <img src={image} alt='product' className='w-25 me-3 ms-3' />

        <div className='m-4 text-center'>
          <h2 className='fw-bold'>{title}</h2>
          <span>{description}</span> <br />
          <h4 className='text-danger fw-bold'>Price: ${price}</h4>
          <span><button className='me-1' onClick={this.decreaseQuantity} >-</button>Qnt: {orderQnt} <button className='ms-1' onClick={this.increaseQuantity} >+</button></span> <br />
          <div className='d-flex justify-content-around align-items-center mt-3'>
            <button className='btn-primary m-2' onClick={this.updateSelector}>Edit</button>

            <button className='btn-primary m-2'>Add to Cart</button>

            <button onClick={this.deleteProduct} className='btn-primary m-2'>Delete</button>

          </div>
        </div>
      </div>) :

      <div className='d-flex flex-column flex-lg-row justify-content-center align-items-center mt-3 decCard'>

        <img src={image} alt='product' className='imageSize me-3 ms-3' />

        <div className='m-4 text-center cardCont'>
          <h2 className='fw-bold'>{title}</h2>
          <span>{description}</span> <br />
          <h4 className='text-danger fw-bold'>Price: ${price}</h4>
          <span><button className='me-1' onClick={this.decreaseQuantity} >-</button>Qnt: {orderQnt} <button className='ms-1' onClick={this.increaseQuantity} >+</button></span> <br />
        </div>

        <div className='m-4 cardCont'>
          <label htmlFor='title' className='fw-bold w-100 m-1'>Title</label>
          <input type='text' id='title' className='fw-bold w-100 m-1 p-1' value={title} onChange={this.changeTitle} />

          <label htmlFor='description' className='fw-bold w-100 m-1'>Description</label>
          <input id='description' type='text' className='w-100 m-1 p-1' value={description} onChange={this.changeDescription} />

          <label htmlFor='price' className='fw-bold w-100 m-1'>Price</label>
          <input id='price' type='number' className='text-danger fw-bold w-100 m-1 p-1' value={price} onChange={this.changePrice} />

          <label htmlFor='image' className='fw-bold w-100 m-1'>Image URL</label>
          <input id='image' type='url' value={image} className='w-100 m-1 p-1' onChange={this.changeImage} />

          <label htmlFor='category' className='fw-bold w-100 m-1'>Category</label>
          <input id='category' type='text' value={category} className='w-100 m-1 p-1' onChange={this.changeCategory} />

          <div className='d-flex justify-content-around align-items-center mt-3'>
            <button className='btn-primary mt-2' onClick={this.SaveUpdate}>Update</button>

            <button className='btn-primary mt-2'>Restore</button>

            <button onClick={this.deleteProduct} className='btn-primary mt-2'>Delete</button>

          </div>
        </div>

      </div>


    const displayPage = this.props.productDeleted ? <Redirect to='/products' /> : subEditor

    const render = Object.keys(productDetails).length > 0 ? displayPage : <Nodata />

    const finalRender = isLoading ? <div className='d-flex justify-content-center align-items-center vh-75'> <Loader /> </div> : fetchSuccesfull ? render : <Errpage message={errMessage} />

    return this.props.processing ? <div className='d-flex justify-content-center align-items-center vh-100'> <Loader /> </div> : finalRender



  }
}

export default ProductItem