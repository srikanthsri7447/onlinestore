import React, { Component } from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Header from './components/Header/Header';
import Home from './components/Home/Home';
import Products from './components/Products/Products';
import ProductItem from './components/ProductItem/ProductItem';
import Signup from './components/Signup/Signup';
import NotFound from './components/NotFound';
import axios from 'axios'
import './App.css';


class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      productsData: [],
      fetchSuccessful: false,
      isLoading: true,
      errMessage: '',
      isLoggedIn: false,
      quantity: {},
      addProduct: false,
      title: '',
      price: '',
      description: '',
      image: '',
      category: '',
      productDeleted: false,
      selector: null,
      processing: false,

    }
  }

  addProductDetailsInput = (name, value) => {

    switch (name) {
      case 'title':
        this.setState({
          title: value
        })
        break;
      case 'price':
        this.setState({
          price: value
        })
        break;
      case 'description':
        this.setState({
          description: value
        })
        break;
      case 'image':
        this.setState({
          image: value
        })
        break;
      case 'category':
        this.setState({
          category: value
        })
        break;
      default:
        return
    }

  }

  deleteProductFn = (id) => {
    this.setState({
      processing: true
    })
    axios.delete(`https://fakestoreapi.com/products/${id}`)
      .then((res) => res.data.id)
      .then((data) => {
        const updatedData = this.state.productsData.filter((eachProduct) => eachProduct.id !== data)

        this.setState({
          productsData: updatedData,
          productDeleted: true,
          processing: false
        })

      })
      .catch(console.log)
  }

  addNewProduct = (data) => {
   
    axios.post('https://fakestoreapi.com/products', (data))
      .then((res) => res.data)
      .then((data) => {
        this.setState({
          productsData: [...this.state.productsData, data],
          addProduct: false,
          title: '',
          price: '',
          description: '',
          image: '',
          category: '',
          processing:false
        })
      })
      .catch(console.log)

  }

  onSaveFn = () => {

    this.setState({
      processing : true
    })
    const newProduct = {
      title: this.state.title,
      price: this.state.price,
      description: this.state.description,
      image: this.state.image,
      category: this.state.category
    }

    this.addNewProduct(newProduct)

  }

  productSelected = () => {
    this.setState({
      productDeleted: false
    })
  }

  getSelector = (id) => {
    const data = this.state.productsData.filter((eachProduct) => eachProduct.id === Number(id))
    const result = data.pop()
    this.setState({
      selector: result,
      updateTitle: result.title,
      updateDescription: result.description,
      updatePrice: result.price,
      updateImage: result.image,
      updateCategory: result.category
    })
  }

  updateTitle = (id, value) => {

    const data = this.state.productsData.map((eachProduct) => {
      if (eachProduct.id === Number(id)) {
        eachProduct.title = value
        return eachProduct
      }
      return eachProduct
    })
    this.setState({
      productsData: data,
    })

  }

  updateDescription = (id, value) => {

    const data = this.state.productsData.map((eachProduct) => {
      if (eachProduct.id === id) {
        eachProduct.description = value
        return eachProduct
      }
      return eachProduct
    })
    this.setState({
      productsData: data
    })
  }

  updatePrice = (id, value) => {

    const data = this.state.productsData.map((eachProduct) => {
      if (eachProduct.id === id) {
        eachProduct.price = value
        return eachProduct
      }
      return eachProduct
    })

    this.setState({
      productsData: data
    })
  }

  updateImage = (id, value) => {

    const data = this.state.productsData.map((eachProduct) => {
      if (eachProduct.id === id) {
        eachProduct.image = value
        return eachProduct
      }
      return eachProduct
    })

    this.setState({
      productsData: data
    })
  }

  updateCategory = (id, value) => {

    const data = this.state.productsData.map((eachProduct) => {
      if (eachProduct.id === id) {
        eachProduct.category = value
        return eachProduct
      }
      return eachProduct
    })

    this.setState({
      productsData: data
    })
  }



  saveUpdate = (id) => {
    this.setState({
      processing:true
    })
    const data = this.state.productsData.filter((eachProduct)=> eachProduct.id === Number(id))

    axios.put(`https://fakestoreapi.com/products/${id}`,data)
      .then((res) => res)
      .then(() => {
        this.setState({
          selector: null,
          processing:false
        })
      })

  }


  componentDidMount = () => {
    this.getProductsData()
  }

  getProductsData = () => {
    axios.get("https://fakestoreapi.com/products")
      .then((res) => res.data)
      .then((data) => {
        this.setState({
          productsData: data,
          fetchSuccessful: true,
          isLoading: false
        })
      }).catch((err) => {
        this.setState({
          isLoading: false,
          errMessage: err.message
        })
      })
  }


  updateLogger = () => {
    this.setState({
      isLoggedIn: true
    })
  }

  updateToggleLogger = () => {
    const isLoggedIn = this.state.isLoggedIn

    if (isLoggedIn) {
      this.setState({
        isLoggedIn: false
      })
    }
  }

  changeQuantity = (id, value) => {
    this.setState((prevState) => {
      prevState.quantity[id] = value
    })
  }

  addProductFn = () => {
    this.setState({
      addProduct: true
    })
  }

  render() {

    const { isLoggedIn, quantity, productsData, fetchSuccessful, isLoading, errMessage, addProduct, productDeleted, selector,processing } = this.state
    const productsDetails = [productsData, fetchSuccessful, isLoading, errMessage, addProduct]

    return (
      <div className="App">
        <BrowserRouter>
          <Header isLogged={isLoggedIn} updateToggleLogger={this.updateToggleLogger} />
          <Switch>
            <Route exact path='/' component={Home} />

            <Route exact path='/products' render={() =>
              <Products
                products={productsDetails}
                addProductFn={this.addProductFn}
                addProductDetailsInput={this.addProductDetailsInput}
                onSaveFn={this.onSaveFn}
                productSelected={this.productSelected}
                processing = {processing}
                 />}
               
                 />

            <Route exact path="/product/:id" render={(props) =>
              <ProductItem
                changeQuantity={this.changeQuantity}
                orderQuantity={quantity}
                productDeleted={productDeleted}
                deleteProductFn={this.deleteProductFn}
                productsData={productsData}
                selector={selector}
                updateSelector={this.getSelector}
                updateTitle={this.updateTitle}
                updateDescription={this.updateDescription}
                updatePrice={this.updatePrice}
                updateImage={this.updateImage}
                updateCategory={this.updateCategory}
                saveUpdate={this.saveUpdate}
                processing={processing}

                {...props} />} />

            <Route exact path='/signup' render={() => <Signup changeLogger={this.updateLogger} />} />
            <Route component={NotFound} />
          </Switch>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
